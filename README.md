Yii2 Config - Faster acces params.
=================

## Description

It was nightmare for me to use the Yii:$app->params[xxx], and i create ad Config class to do this.

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/). 

To install, either run

```
$ composer require buibr/yii2config "*"
```
or add

```
$ "buibr/yii2config":"*"
```

to the ```require``` section of your `composer.json` file.

## Usage

Example if the ```params.php``` contains this: 

```php
    ...
    'mail'=>[
        ...
        'contact'   => 'buibr@....',
        'from_name' => 'Marketing Team',
        ...
    ]
    ...

```
then: 
```php
Config::param('mail.contact');
```
For every ```.``` means sub element of array in the params.
If this elemet is not found will be returned false.

## License

**buibr/yii2config** is released under the MIT License. See the bundled `LICENSE` for details.
