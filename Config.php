<?php


namespace buibr\yii2config;

use yii\helpers\Url;
use yii\helpers\FileHelper;


/**
 * THis class only returns the config params from ```config/params.php``` but in easy way
 * use (.) dot to acces subarrays in unlimited subelements.
 * 
 * @method p
 * - config/params.php
 * @method a
 * - aliases
 * @method d
 * - directories 
 */
class Config {

    /**
     * Gets parametters from config/params.php 
     */
    public static function param($key = null){

        if(empty($key)) {
            throw new \ErrorException("Undefined parametter.", 100);
        }

        if(isset(\Yii::$app->params[trim($key)])) {
            return \Yii::$app->params[$key];
        }

        //  split params with dods as arrays and subarray
        //  ex burhan.ibrahimi will be like \Yii::$app->param['burhan']['ibrahimi']
        $split = ( strpos($key, '.') > -1 ) ? explode('.', $key) : $key;

        //  if string means that this key has no . in it and is not looking for array.
        if(\is_string($split))
        {
            return false;
        }

        if( !isset( \Yii::$app->params[trim($split[0])] ))
        {
            return false;
        }
        
        if( !isset( \Yii::$app->params[$split[0]][$split[1]] ))
        {
            return false;
        }

        if(count($split) == 2)
        {
            return \Yii::$app->params[$split[0]][$split[1]];
        }

        if( !isset( \Yii::$app->params[$split[0]][$split[1]][$split[2]] ))
        {
            return false;
        }

        if(count($split) == 3)
        {
            return \Yii::$app->params[$split[0]][$split[1]][$split[2]];
        }

        if( !isset( \Yii::$app->params[$split[0]][$split[1]][$split[2]][$split[3]] ))
        {
            return false;
        }

        if(count($split) == 3)
        {
            return \Yii::$app->params[$split[0]][$split[1]][$split[2]][$split[3]];
        }

        return false;
    }

    /**
     * Gets parametters from config/params.php 
     */
    public static function p($key = null){
        return self::param($key);
    }

    /**
     * Return full directory from alias and given params
     * Example you have parameter like
     * ```
     * [
     *      ...
     *      'list' => [
     *          ...
     *          item1 = 'images/'
     *          ...
     *      ]
     * ]
     * ```
     * and passing:
     * ```
     *      Config::d('list.item1', '@app');
     * ```
     * will return:
     * ```
     *      /var/www/html/images/
     * ```
     * 
     * @param string $param
     * @param string $aliase
     * @return string
     */
    public static function dir($param, $aliase = null){

        if(!empty($aliase) && $var = self::param($param)) {
            return \Yii::getAlias($aliase . $var);
        }
        elseif($var = self::param($param)) {
            return \Yii::getAlias($var);
        }

        return null;
    }


    /**
     * @dir
     */
    public static function d($param, $base = null){
        return self::dir($param, $base);
    }

    /**
     * Combination of @webroot alias with any other parametter.
     * returns 
     */
    public static function webroot($param){

        if($var = self::param($param)){
            $dir =  \Yii::getAlias("@webroot{$var}");
        }

        if(!is_dir(($dir))){
            @FileHelper::createDirectory($dir);
        }

        return $dir;
    }

    /**
     * Combination of Url::base(true) . param
     */
    public static function base($param){
        
        if($var = self::param($param)){
            return Url::base(true) . $var;
        }

        return null;
    }


    /**
     * Combine alias with static string
     */
    public static function alias($string){
        return \Yii::getAlias($string);
    }

    /**
     * Combine alias with static string
     */
    public static function a($string){
        return self::alias($string);
    }
    

}